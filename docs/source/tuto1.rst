
Tutorial: Picturing a conjunction 
---------------------------------

.. image:: ../../example/conjunction/screenshot.png


.. highlight:: python
   :linenothreshold: 5
   
.. literalinclude:: ../../example/conjunction/oem_from_cdm.py
   


See :download:`this script <../../example/conjunction/oem_from_cdm.py>`.

