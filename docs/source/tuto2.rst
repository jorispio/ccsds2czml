
Tutorial: Multiple OEM plot
----------------------------

.. image:: ../../example/constellation/screenshot.png


.. highlight:: python
   :linenothreshold: 5
   
.. literalinclude:: ../../example/constellation/constellation_oems.py
   

See :download:`this script <../../example/constellation/constellation_oems.py>`.

