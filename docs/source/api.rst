
Python API 
============

.. automodule:: oacmpy
   :members:

.. automodule:: oacmpy.ccsds
   :members:
   :undoc-members:
   
CCSDS classes
-------------

.. automodule:: oacmpy.ccsds.Aem
   :members:
   :undoc-members:
      

.. automodule:: oacmpy.ccsds.Apm
   :members:
   
.. automodule:: oacmpy.ccsds.Cdm
   :members:
   
.. automodule:: oacmpy.ccsds.Oem
   :members:
   
.. automodule:: oacmpy.ccsds.Omm
   :members:
   
.. automodule:: oacmpy.ccsds.Opm
   :members:

Data types 
----------
.. automodule:: oacmpy.ccsds.CommonOrbitData
   :members:
   :inherited-members:
       
.. automodule:: oacmpy.ccsds.CovarianceParameters
   :members:
   :inherited-members:   
   
CZML
-----

.. automodule:: oacmpy.czml
   :members:

.. automodule:: oacmpy.czml.CdmToCzml
   :members:
   
.. autoclass:: CdmToCzml
   :members:
   
.. automodule:: oacmpy.czml.OemAemToCzml
   :members:   

.. automodule:: oacmpy.czml.OemToCzml
   :members:   


.. automodule:: oacmpy.czml.OemToCzml
   :members:  


.. automodule:: oacmpy.czml.ScenarioCzml
   :members:      
   
.. automodule:: oacmpy.czml.CzmlUtils
   :members:
   
   
Utilities 
----------

.. automodule:: oacmpy.frames
   :show-inheritance:
   :members:

.. automodule:: oacmpy.utils.rotation
   :show-inheritance:
   :members:   

.. autofunction:: dcm_to_quat

.. autofunction:: angles2matrix


