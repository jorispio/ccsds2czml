.. oacmpy documentation master file, created by
   sphinx-quickstart on Sat Apr  9 15:07:26 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to oacmpy's documentation!
==================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   api
    

Tutorials
---------

.. toctree::
   :maxdepth: 2
   :caption: Tutorials
   :name: tutos

   tuto1
   tuto2


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
