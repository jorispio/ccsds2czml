"""


"""
import pathlib
from distutils.core import setup

from setuptools import find_packages

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "./README.md").read_text()


setup(
    name="oacmpy",
    version="0.0.3",
    author="Joris T. OLYMPIO",
    author_email="jto.devs@gmail.com",
    description="""Parse CCSDS Data Message, including OEM, AEM and CDM, and convert them to a CZML scenario file.""",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jorispio/ccsds2czml",
    license="MIT",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    keywords='"space trajectory" orbit',
    python_requires=">=3.6",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    package_data={"": ["data/*.dat", "data/*.txt", "data/*.all"]},
    install_requires=[],
)
