import os
import unittest

import oacmpy
from oacmpy import ccsds
from oacmpy.ccsds.enumerates import InterpolationMethod, ReferenceFrame, TimeSystem

DATA_DIR = os.path.join(oacmpy.CCSDS_DIR, "../../tests/data")


class TestOemParser(unittest.TestCase):
    def test_oem_xml(self):
        message = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.xml"), "xml")
        self.assertEqual(message.get_version(), "2.0")

        self.assertEqual(message.get_satellite_count(), 1)
        self.assertEqual(list(message.get_satellite_list()), ["CHEOPS"])

        self.assertEqual(message.get_object_id("CHEOPS"), "2019-015B")
        self.assertEqual(message.get_center_name("CHEOPS"), "EARTH")
        self.assertEqual(message.get_reference_frame("CHEOPS"), ReferenceFrame.EME2000)
        self.assertEqual(message.get_time_system("CHEOPS"), TimeSystem.UTC)

        self.assertEqual(
            message.get_start_time("CHEOPS").str(), "2019-06-01T00:00:00.000000Z"
        )
        self.assertEqual(
            message.get_stop_time("CHEOPS").str(), "2019-06-01T01:00:00.000000Z"
        )
        self.assertEqual(
            message.get_interpolation_method("CHEOPS"),
            InterpolationMethod.LAGRANGE,
        )
        self.assertEqual(message.get_interpolation_degree("CHEOPS"), 8)

        ephemeris = message.get_ephemeris("CHEOPS")
        self.assertEqual(len(ephemeris), 13)
        state = ephemeris[0]
        self.assertEqual(state[0].str(), "2019-06-01T00:00:00.000000Z")
        pv = state[1]
        self.assertAlmostEqual(pv[0], -5335528.049454)
        self.assertAlmostEqual(pv[1], 2864468.099890)
        self.assertAlmostEqual(pv[2], -3671843.346056)
        self.assertAlmostEqual(pv[3], 3977.354768000)
        self.assertAlmostEqual(pv[4], -710.873464000)
        self.assertAlmostEqual(pv[5], -6321.330625000)

    def test_oem_cov_xml(self):
        message = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem_with_cov.xml"), "xml")
        self.assertEqual(message.get_version(), "2.0")

        object_name = "MARS GLOBAL SURVEYOR"
        self.assertEqual(message.get_satellite_count(), 1)
        self.assertEqual(list(message.get_satellite_list()), [object_name])

        self.assertEqual(message.get_object_id(object_name), "1996-062A")
        self.assertEqual(message.get_center_name(object_name), "MARS BARYCENTER")
        self.assertEqual(
            message.get_reference_frame(object_name), ReferenceFrame.EME2000
        )
        self.assertEqual(message.get_time_system(object_name), TimeSystem.UTC)

        self.assertEqual(
            message.get_start_time(object_name).str(), "1996-12-18T12:00:00.331000Z"
        )
        self.assertEqual(
            message.get_stop_time(object_name).str(), "1996-12-28T21:28:00.331000Z"
        )
        self.assertEqual(
            message.get_interpolation_method(object_name),
            InterpolationMethod.HERMITE,
        )
        self.assertEqual(message.get_interpolation_degree(object_name), 7)

        ephemeris = message.get_ephemeris(object_name)
        self.assertEqual(len(ephemeris), 4)
        state = ephemeris[0]
        self.assertEqual(state[0].str(), "1996-12-18T12:00:00.331000Z")
        pv = state[1]
        self.assertAlmostEqual(pv[0], 2789600.0)
        self.assertAlmostEqual(pv[1], -280000.0)
        self.assertAlmostEqual(pv[2], -1746800.0)
        self.assertAlmostEqual(pv[3], 4730.0)
        self.assertAlmostEqual(pv[4], -2500.0)
        self.assertAlmostEqual(pv[5], -1040.0)

        # self.assertEqual(
        #    message.get_covariances_frame(object_name), ReferenceFrame.ITRF97
        # )
        covariances = message.get_covariances(object_name)
        self.assertEqual(len(covariances), 1)

        covariance = covariances[0]
        # self.assertEqual(covariance["EPOCH"], "2019-06-01T00:05:00.000")
        self.assertEqual(
            ReferenceFrame(covariance["COV_REF_FRAME"]), ReferenceFrame.ITRF97
        )
        self.assertEqual(covariance.get_reference_frame(), ReferenceFrame.ITRF97)
        matrix = covariance["matrix"]
        self.assertAlmostEqual(matrix[0][0], 0.316)
        self.assertAlmostEqual(matrix[1][0], 0.722)
        self.assertAlmostEqual(matrix[1][1], 0.518)
        self.assertAlmostEqual(matrix[2][0], 0.202)
        self.assertAlmostEqual(matrix[2][1], 0.715)
        self.assertAlmostEqual(matrix[2][2], 0.002)
        self.assertAlmostEqual(matrix[3][0], 0.912)
        self.assertAlmostEqual(matrix[3][1], 0.306)
        self.assertAlmostEqual(matrix[3][2], 0.276)
        self.assertAlmostEqual(matrix[3][3], 0.797)
        self.assertAlmostEqual(matrix[4][0], 0.562)
        self.assertAlmostEqual(matrix[4][1], 0.899)
        self.assertAlmostEqual(matrix[4][2], 0.022)
        self.assertAlmostEqual(matrix[4][3], 0.079)
        self.assertAlmostEqual(matrix[4][4], 0.415)
        self.assertAlmostEqual(matrix[5][0], 0.245)
        self.assertAlmostEqual(matrix[5][1], 0.965)
        self.assertAlmostEqual(matrix[5][2], 0.950)
        self.assertAlmostEqual(matrix[5][3], 0.435)
        self.assertAlmostEqual(matrix[5][4], 0.621)
        self.assertAlmostEqual(matrix[5][5], 0.991)

    def test_oem_kvn(self):
        message = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.oem"), "kvn")
        self.assertEqual(message.get_version(), "2.0")

        self.assertEqual(message.get_satellite_count(), 1)
        self.assertEqual(list(message.get_satellite_list()), ["MARS GLOBAL SURVEYOR"])

        self.assertEqual(message.get_object_id("MARS GLOBAL SURVEYOR"), "1996-062A")
        self.assertEqual(
            message.get_center_name("MARS GLOBAL SURVEYOR"), "MARS BARYCENTER"
        )
        self.assertEqual(
            message.get_reference_frame("MARS GLOBAL SURVEYOR"),
            ReferenceFrame.EME2000,
        )
        self.assertEqual(
            message.get_time_system("MARS GLOBAL SURVEYOR"), TimeSystem.UTC
        )

        self.assertEqual(
            message.get_interpolation_method("MARS GLOBAL SURVEYOR"),
            InterpolationMethod.HERMITE,
        )
        self.assertEqual(message.get_interpolation_degree("MARS GLOBAL SURVEYOR"), 7)

        ephemeris = message.get_ephemeris("MARS GLOBAL SURVEYOR")
        self.assertEqual(len(ephemeris), 8)
        state = ephemeris[0]
        self.assertEqual(state[0].str(), "1996-12-18T12:00:00.331000Z")
        pv = state[1]
        self.assertAlmostEqual(pv[0], 2789619.0)
        self.assertAlmostEqual(pv[1], -280045.0)
        self.assertAlmostEqual(pv[2], -1746755.0)
        self.assertAlmostEqual(pv[3], 4733.72)
        self.assertAlmostEqual(pv[4], -2495.86)
        self.assertAlmostEqual(pv[5], -1041.95)

        state = ephemeris[3]
        self.assertEqual(state[0].str(), "1996-12-28T21:28:00.331000Z")
        pv = state[1]
        self.assertAlmostEqual(pv[0], -3881024.0)
        self.assertAlmostEqual(pv[1], 563959.0)
        self.assertAlmostEqual(pv[2], -682773)
        self.assertAlmostEqual(pv[3], -3288.27)
        self.assertAlmostEqual(pv[4], -3667.35)
        self.assertAlmostEqual(pv[5], 1638.61)

    def test_oem_cov_kvn(self):
        message = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem_with_cov.oem"), "kvn")
        self.assertEqual(message.get_covariances_frame("CHEOPS"), ReferenceFrame.TNW)
        covariances = message.get_covariances("CHEOPS")
        self.assertEqual(len(covariances), 6)

        covariance = covariances[1]
        self.assertEqual(covariance["EPOCH"], "2019-06-01T00:05:00.000")
        matrix = covariance["matrix"]
        self.assertAlmostEqual(matrix[0][0], 3.1778591e-03)
        self.assertAlmostEqual(matrix[1][0], 3.9966767e-05)
        self.assertAlmostEqual(matrix[1][1], 1.1815466e-06)
        self.assertAlmostEqual(matrix[2][0], 1.4321323e-06)
        self.assertAlmostEqual(matrix[2][1], 1.8785173e-08)
        self.assertAlmostEqual(matrix[2][2], 1.9193015e-06)
        self.assertAlmostEqual(matrix[3][0], -3.7949216e-08)
        self.assertAlmostEqual(matrix[3][1], 1.8864408e-10)
        self.assertAlmostEqual(matrix[3][2], -1.6758687e-11)
        self.assertAlmostEqual(matrix[3][3], 1.1269109e-12)
        self.assertAlmostEqual(matrix[4][0], 3.2929218e-06)
        self.assertAlmostEqual(matrix[4][1], 4.4246320e-08)
        self.assertAlmostEqual(matrix[4][2], 1.4998879e-09)
        self.assertAlmostEqual(matrix[4][3], -3.7245083e-11)
        self.assertAlmostEqual(matrix[4][4], 3.4768280e-09)
        self.assertAlmostEqual(matrix[5][0], -1.6245080e-10)
        self.assertAlmostEqual(matrix[5][1], -1.5871816e-12)
        self.assertAlmostEqual(matrix[5][2], -2.8717703e-11)
        self.assertAlmostEqual(matrix[5][3], 1.4490821e-15)
        self.assertAlmostEqual(matrix[5][4], -1.6927863e-13)
        self.assertAlmostEqual(matrix[5][5], 2.1861510e-12)

    def test_oem_kvn_frac_ms99999(self):
        # test issue #3 fix
        message = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_99999.oem"), "kvn")
        self.assertEqual(message.get_version(), "3.0")

        self.assertEqual(
            message.get_start_time("SN1").str(), "2022-10-01T01:59:59.999999Z"
        )

        ephemeris = message.get_ephemeris("SN1")
        state = ephemeris[9]  # 10th
        self.assertEqual(state[0].str(), "2022-10-01T02:09:00.000000Z")
        pv = state[1]
        self.assertAlmostEqual(pv[0], -5220646.496538615)
        self.assertAlmostEqual(pv[1], 2465445.9289372917)
        self.assertAlmostEqual(pv[2], 3826788.851238029)
        self.assertAlmostEqual(pv[3], 4258.070577690157)
        self.assertAlmostEqual(pv[4], -680.8490104993219)
        self.assertAlmostEqual(pv[5], 6238.286039523919)

    def test_oem_cov_kvn(self):
        # test issue #4 fix
        message = ccsds.Oem.Oem(
            os.path.join(DATA_DIR, "test_oem_with_cov_fix.oem"), "kvn"
        )
        self.assertEqual(message.get_covariances_frame("CHEOPS"), ReferenceFrame.TNW)
        covariances = message.get_covariances("CHEOPS")
        self.assertEqual(len(covariances), 4)

        covariance = covariances[1]
        self.assertEqual(covariance["EPOCH"], "2019-06-01T00:05:00.000")
        matrix = covariance["matrix"]
        self.assertAlmostEqual(matrix[0][0], 3.1778591e-03)
        self.assertAlmostEqual(matrix[1][0], 3.9966767e-05)
        self.assertAlmostEqual(matrix[1][1], 1.1815466e-06)
        self.assertAlmostEqual(matrix[2][0], 1.4321323e-06)
        self.assertAlmostEqual(matrix[2][1], 1.8785173e-08)
        self.assertAlmostEqual(matrix[2][2], 1.9193015e-06)
        self.assertAlmostEqual(matrix[3][0], -3.7949216e-08)
        self.assertAlmostEqual(matrix[3][1], 1.8864408e-10)
        self.assertAlmostEqual(matrix[3][2], -1.6758687e-11)
        self.assertAlmostEqual(matrix[3][3], 1.1269109e-12)
        self.assertAlmostEqual(matrix[4][0], 3.2929218e-06)
        self.assertAlmostEqual(matrix[4][1], 4.4246320e-08)
        self.assertAlmostEqual(matrix[4][2], 1.4998879e-09)
        self.assertAlmostEqual(matrix[4][3], -3.7245083e-11)
        self.assertAlmostEqual(matrix[4][4], 3.4768280e-09)
        self.assertAlmostEqual(matrix[5][0], -1.6245080e-10)
        self.assertAlmostEqual(matrix[5][1], -1.5871816e-12)
        self.assertAlmostEqual(matrix[5][2], -2.8717703e-11)
        self.assertAlmostEqual(matrix[5][3], 1.4490821e-15)
        self.assertAlmostEqual(matrix[5][4], -1.6927863e-13)
        self.assertAlmostEqual(matrix[5][5], 2.1861510e-12)


if __name__ == "__main__":
    unittest.main()
