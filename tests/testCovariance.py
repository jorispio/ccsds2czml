import importlib
import os
import unittest

import numpy as np
import oacmpy
from oacmpy import ccsds
from oacmpy.ccsds.enumerates import ConjunctionObject, ReferenceFrame
from oacmpy.czml.CdmToCzml import Covariance, RtnMatrix
from oacmpy.datetime.Date import Date

matplotlib_mod = importlib.util.find_spec("matplotlib")
matplotlib_mod_found = matplotlib_mod is not None
if matplotlib_mod_found:
    import matplotlib.pyplot as plt


DATA_DIR = os.path.join(oacmpy.CCSDS_DIR, "../../tests/data")
DEFAULT_DATE = Date("2000-01-01T00:00:00.000Z")


class TestCovarianceCzml(unittest.TestCase):
    def test_covariance_transform_rtn_eci1(self):
        date = DEFAULT_DATE
        cov = Covariance(
            np.eye(6, 6), [1, 0, 0, 0, 1, 0], date, ReferenceFrame.ITRF2000
        )
        q = cov.get_orientation_quaternion_ecf()
        self.assertEqual(q[3], 1.0)

    def test_covariance_transform_rtn_eci2(self):
        date = DEFAULT_DATE
        rv_eci = [2570097.0, 2244656.9, 6281497.9, 4418.8, 44833.5, -3526.8]
        cov_rtn = np.eye(6, 6)
        cov = Covariance(cov_rtn, rv_eci, date, ReferenceFrame.ITRF2000)
        m_rtn = cov.get_orientation_matrix()
        m_eci = cov.get_orientation_matrix_ecf()

        axis_1_in_rtn = np.dot(m_rtn, [1, 0, 0])
        axis_2_in_rtn = np.dot(m_rtn, [0, 1, 0])
        axis_3_in_rtn = np.dot(m_rtn, [0, 0, 1])
        self.assertListEqual(list(axis_1_in_rtn), [1, 0, 0])
        self.assertListEqual(list(axis_2_in_rtn), [0, 1, 0])
        self.assertListEqual(list(axis_3_in_rtn), [0, 0, 1])

        axis_1_in_eci = np.dot(m_eci, [1, 0, 0])
        axis_2_in_eci = np.dot(m_eci, [0, 1, 0])
        axis_3_in_eci = np.dot(m_eci, [0, 0, 1])
        ur = np.array(rv_eci[0:3]) / np.linalg.norm(rv_eci[0:3])
        self.assertListEqual(list(axis_1_in_eci), list(ur))

    @unittest.skip
    def test_cdm_czml(self):
        message = ccsds.Cdm.Cdm(os.path.join(DATA_DIR, "test_cdm.xml"), "xml")
        date = message.get_tca()

        cov_rtn = message.get_covariance(ConjunctionObject.OBJECT1)
        rv1_eci = message.get_state_vector(ConjunctionObject.OBJECT1)
        frame1 = message.get_coordinate_system(ConjunctionObject.OBJECT1)
        self.assertEqual(frame1, ReferenceFrame.EME2000)
        cov1 = Covariance(
            cov_rtn, rv1_eci, date
        )  # omit frame for this test on purpose, to use dr_eci with m_eci_rth
        radii1 = cov1.get_axis() * 3.0
        p1_rtn = cov1.get_orientation_matrix()  # Ellipsoid axis -> RTN

        cov_rtn = message.get_covariance(ConjunctionObject.OBJECT2)
        rv2_eci = message.get_state_vector(ConjunctionObject.OBJECT2)
        frame2 = message.get_coordinate_system(ConjunctionObject.OBJECT2)
        self.assertEqual(frame2, ReferenceFrame.EME2000)
        cov2 = Covariance(
            cov_rtn, rv1_eci, date
        )  # omit frame for this test on purpose, to use dr_eci with m_eci_rth
        radii2 = cov2.get_axis() * 3.0
        p2_rtn = cov2.get_orientation_matrix()  # Ellipsoid axis -> RTN

        dr_rtn_cdm = message.get_relative_state()
        dr_eci = np.array(rv2_eci[0:3]) - np.array(rv1_eci[0:3])
        dr_rtn = np.dot(np.transpose(cov1.get_transform_rtn2ecf()), dr_eci)
        print("Relative Position RTN: ", dr_rtn_cdm)
        print("Relative Position RTN : {} (recomputed)".format(dr_rtn))
        self.assertAlmostEqual(dr_rtn_cdm[0], dr_rtn[0], delta=0.1)
        self.assertAlmostEqual(
            dr_rtn_cdm[1], dr_rtn[1], delta=25.0
        )  # FIXME(joris): error sensibly large
        self.assertAlmostEqual(dr_rtn_cdm[2], dr_rtn[2], delta=3.0)

        x1, y1, z1, maxlen1 = self.get_covariance_points(
            p1_rtn, radii1, [0.0, 0.0, 0.0]
        )
        x2, y2, z2, maxlen2 = self.get_covariance_points(p2_rtn, radii2, dr_rtn)
        # maxlen = max(maxlen1, maxlen2)

        fig = plt.figure(figsize=(8, 8))
        ax = fig.gca(projection="3d")
        ax.plot_surface(
            x1, y1, z1, rstride=3, cstride=3, linewidth=0.1, alpha=0.5, shade=True
        )
        ax.plot_surface(
            x2, y2, z2, rstride=3, cstride=3, linewidth=0.1, alpha=0.5, shade=True
        )
        # s = "Miss distance: {:.1f} m".format(np.linalg.norm(dr_rtn))
        # plt.text(0.1, 0.1, s, fontsize=12)
        # ax.set_xlim3d([-maxlen, maxlen])
        # ax.set_ylim3d([-maxlen, maxlen])
        # ax.set_zlim3d([-maxlen, maxlen])
        ax.set_xlabel("R, m")
        ax.set_ylabel("T, m")
        ax.set_zlabel("N, m")
        ax.set_title(
            "Conjunction geometry in the RTN frame of OBJECT1 (3-sigma covariance ellipsoid)"
        )
        fig.tight_layout()
        plt.grid()
        plt.show()

    @staticmethod
    def get_covariance_points(p_rtn, radii, center):
        # cartesian coordinates for the ellipsoid surface
        u = np.linspace(0.0, 2.0 * np.pi, 60)
        v = np.linspace(0.0, np.pi, 60)
        x = radii[0] * np.outer(np.cos(u), np.sin(v))
        y = radii[1] * np.outer(np.sin(u), np.sin(v))
        z = radii[2] * np.outer(np.ones_like(u), np.cos(v))

        maxlen = np.max(radii) * 1.01

        rotation = p_rtn
        for i in range(len(x)):
            for j in range(len(x)):
                [x[i, j], y[i, j], z[i, j]] = (
                    np.dot([x[i, j], y[i, j], z[i, j]], rotation) + center
                )

        return x, y, z, maxlen
