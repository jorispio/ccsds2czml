import datetime as dt
import os
import unittest
from datetime import timedelta

import oacmpy
from oacmpy import ccsds
from oacmpy.ccsds.exceptions import CcsdsError
from oacmpy.czml import CdmToCzml, OemToCzml, ScenarioCzml

DATA_DIR = os.path.join(oacmpy.CCSDS_DIR, "../../tests/data")


class TestOemParser(unittest.TestCase):
    def test_czml_from_oem(self):
        oem = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.oem"), "kvn")
        czml = OemToCzml.OemAemToCzml(oem)
        czml.dump()

    def test_czml_from_oem_aem(self):
        oem = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.oem"), "kvn")
        aem = ccsds.Aem.Aem(os.path.join(DATA_DIR, "test_aem.aem"), "kvn")
        czml = OemToCzml.OemAemToCzml(oem, aem)
        czml.dump()

    def test_scenario_trajectory(self):
        oem = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.xml"), "xml")
        czml = OemToCzml.OemAemToCzml(oem)

        start = oem.get_start_time("CHEOPS")
        end = oem.get_stop_time("CHEOPS")

        scenario = ScenarioCzml.ScenarioCzml(start, end)
        scenario.add_content(czml)
        scenario.create_document("test_oem.czml")

    def test_scenario_multiple_satellites(self):
        oem = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.xml"), "xml")
        start = oem.get_start_time("CHEOPS")
        end = oem.get_stop_time("CHEOPS")

        scenario = ScenarioCzml.ScenarioCzml(start, end)
        for _ in range(0, 50):
            czml = OemToCzml.OemAemToCzml(oem)
            scenario.add_content(czml)
        scenario.create_document("test_mult_oem.czml")

    def test_scenario_attitude(self):
        oem = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.xml"), "xml")
        aem = ccsds.Aem.Aem(os.path.join(DATA_DIR, "test_aem_for_oem.aem"), "kvn")
        czml = OemToCzml.OemAemToCzml(oem, aem)

        start = oem.get_start_time("CHEOPS")
        end = oem.get_stop_time("CHEOPS")

        scenario = ScenarioCzml.ScenarioCzml(start, end)
        scenario.add_content(czml)
        scenario.create_document("test_oem_aem.czml")

    def test_scenario_attitude_fail(self):
        oem = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem.xml"), "xml")
        aem = ccsds.Aem.Aem(os.path.join(DATA_DIR, "test_aem.aem"), "kvn")
        with self.assertRaises(CcsdsError):
            OemToCzml.OemAemToCzml(oem, aem).dump()

    @unittest.skip("waiting for next release of czml3")
    def test_czml_from_cdm(self):
        oem1 = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem_a.xml"), "xml")
        oem1_czml = OemToCzml.OemAemToCzml(oem1)
        oem2 = ccsds.Oem.Oem(os.path.join(DATA_DIR, "test_oem_b.xml"), "xml")
        oem2_czml = OemToCzml.OemAemToCzml(oem2)
        cdm = ccsds.Cdm.Cdm(os.path.join(DATA_DIR, "test_cdm_ab.cdm"), "kvn")
        cdm_czml = CdmToCzml.CdmToCzml(cdm)
        cdm_czml.set_lead_time(timedelta(minutes=30))
        cdm_czml.set_scale(10000.0)

        start = oem1.get_start_time("SATELLITE A").datetime
        end = oem1.get_stop_time("SATELLITE A").datetime

        scenario = ScenarioCzml.ScenarioCzml(start, end)
        scenario.add_content(oem1_czml)
        scenario.add_content(oem2_czml)
        scenario.add_content(cdm_czml)
        scenario.create_document("test_oem_cdm.czml")


if __name__ == "__main__":
    unittest.main()
