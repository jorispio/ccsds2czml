import unittest

from dateutil.parser import isoparse as parse_iso_date
from oacmpy.ccsds.enumerates import TimeSystem
from oacmpy.datetime.Date import Date, datetime_to_mjd
from oacmpy.datetime.TimeScale import (
    GMST,
    GPS,
    TAI,
    TCB,
    TCG,
    TDB,
    TT,
    UT1,
    UTC,
    Timescale,
)
from oacmpy.errors import DateError


class TestDateUtils(unittest.TestCase):
    def test_datetime_to_mjd(self):
        d = parse_iso_date("2001-02-03T04:05:06.700")
        day, fod = datetime_to_mjd(d)
        self.assertEqual(day, 51943)
        self.assertAlmostEqual(fod, 0.1702164351851852)


class TestTimescale(unittest.TestCase):
    def setUp(self) -> None:
        Timescale.set_leap_second(37.0)
        Timescale.set_delta_ut1(0.8)

    def test_tai(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.TAI)
        self.assertEqual(TAI.offset_from(date, TAI), 0.0)
        self.assertEqual(TAI.offset_from(date, GPS), 19.0)
        self.assertEqual(TAI.offset_from(date, TT), -32.184)
        self.assertEqual(TAI.offset_from(date, UTC), 37.0)
        self.assertEqual(
            TAI.offset_from(date, UT1), 37.0 - 0.8
        )  # TAI-UT1 = (TAI-UTC) - (UT1-UTC)
        # self.assertEqual(TAI.offset(date, GMST), 0.0)
        self.assertAlmostEqual(
            TAI.offset_from(date, TDB), -32.184977828383886
        )  # (TAI - TT) + (TT - TDB) ~ -32.184 Orekit error?
        self.assertAlmostEqual(TAI.offset_from(date, TCB), -52.29776795142537)

    def test_utc(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.UTC)
        self.assertEqual(UTC.offset_from(date, UTC), 0.0)
        self.assertEqual(UTC.offset_from(date, UT1), -0.8)  # UTC-UT1
        self.assertEqual(UTC.offset_from(date, TAI), -37.0)
        self.assertEqual(
            UTC.offset_from(date, TT), -37.0 - 32.184
        )  # UTC-TT = (UTC-TAI) - (TT-TAI)
        self.assertAlmostEqual(UTC.offset_from(date, TCB), -89.29776795142537)
        self.assertAlmostEqual(UTC.offset_from(date, TDB), -69.1849778283839)

    def test_tt(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.UTC)
        self.assertEqual(TT.offset_from(date, TAI), 32.184)
        self.assertEqual(TT.offset_from(date, UTC), 69.184)
        self.assertEqual(TT.offset_from(date, TDB), -0.0009778283838944412)

    def test_gps(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.UTC)
        self.assertEqual(GPS.offset_from(date, TAI), -19.0)

    def test_gmst_oh(self):
        # see: http://dc.zah.uni-heidelberg.de/apfs/times/q/form for validation
        date_gmst_ref = Date("2018-03-02T10:38:57.1375", TimeSystem.GMST)
        date_utc = Date("2018-03-02T00:00:00.000", TimeSystem.UT1)
        date_gmst = date_utc.change_scale(TimeSystem.GMST)
        self.assertAlmostEqual(date_gmst.duration_from(date_gmst_ref), 0.0, delta=0.1)

        date_gmst_ref = Date("2018-03-02T16:39:56.2763", TimeSystem.GMST)
        date_utc = Date("2018-03-02T06:00:00.000", TimeSystem.UT1)
        date_gmst = date_utc.change_scale(TimeSystem.GMST)
        self.assertAlmostEqual(date_gmst.duration_from(date_gmst_ref), 0.0, delta=0.1)

    def test_gmst(self):
        # see: http://dc.zah.uni-heidelberg.de/apfs/times/q/form for validation
        date = Date("2018-03-02T15:33:00.0", TimeSystem.GMST)  # 04:53:14.508 UTC
        self.assertAlmostEqual(GMST.offset_from(date, TAI), 38349.1, delta=0.1)
        self.assertAlmostEqual(GMST.offset_from(date, UT1), 38385.3, delta=0.1)
        self.assertAlmostEqual(GMST.offset_from(date, UTC), 38386.1, delta=0.1)
        self.assertAlmostEqual(GMST.offset_from(date, TT), 38316.9, delta=0.1)

    def test_tdb(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.TDB)
        self.assertEqual(TDB.offset_from(date, TT), 0.0009778283838944412)

    def test_tcb(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.TCB)
        self.assertEqual(TCB.offset_from(date, TDB), 20.112790123041485)

    def test_tcg(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.TCG)
        self.assertEqual(TCG.offset_from(date, TT), 0.9040285469176966)


class TestDate(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_julian_day(self):
        date = Date("2018-02-08T10:16:27.0Z", TimeSystem.UTC)
        d, fod = date.julian_date()
        self.assertEqual(d, 2458157)
        self.assertEqual(fod, 0.92809027777777775)
        self.assertEqual(date.jd, (2458157, 0.9280902777777778))
        pass

    def test_modified_julian_day(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.UTC)
        d, fod = date.modified_julian_date()
        self.assertEqual(d, 58157)
        self.assertEqual(fod, 0.42809027777777775)
        self.assertEqual(date.mjd_utc, (58157, 0.42809027777777775))
        pass

    def test_julian_century(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.UTC)
        jc = date.julian_century
        self.assertEqual(jc, 0.18105210377214997)

    def test_date_utc_zulu(self):
        d1 = Date("1996-12-18T12:02:00.331000", TimeSystem.UTC)
        d2 = Date("1996-12-18T12:02:00.331000Z")
        self.assertEqual(d1.duration_from(d2), 0.0)

    def test_date_gps(self):
        d1 = Date("2018-12-18T12:02:00.331000Z")
        d2 = Date("2018-12-18T12:02:00.331000", TimeSystem.GPS)
        self.assertAlmostEqual(d2.duration_from(d1), -37.0 + 19.0)

    def test_date_utc_ut1(self):
        d1 = Date("1996-12-18T12:02:00.331000Z")
        d2 = Date("1996-12-18T12:02:00.331000", TimeSystem.UT1)
        self.assertAlmostEqual(d2.duration_from(d1), -0.3341)

    def test_date_gps_tai(self):
        d1 = Date("1996-12-18T12:02:00.331000", TimeSystem.GPS)
        d2 = Date("1996-12-18T12:02:00.331000", TimeSystem.TAI)
        self.assertAlmostEqual(d2.duration_from(d1), -19.0)

    def test_date_utc_tai(self):
        d1 = Date("1996-12-18T12:02:00.331000", TimeSystem.UTC)
        d2 = Date("1996-12-18T12:02:00.331000", TimeSystem.TAI)
        self.assertAlmostEqual(d2.duration_from(d1), -30.0)  # TAI-UTC=  30.0

    def test_date_tt_tai(self):
        d1 = Date("1996-12-18T12:02:00.331000", TimeSystem.TT)
        d2 = Date("1996-12-18T12:02:00.331000", TimeSystem.TAI)
        self.assertAlmostEqual(d2.duration_from(d1), 32.184)

    def test_date_tt_ut1(self):
        d1 = Date("1996-12-18T12:02:00.331000", TimeSystem.TT)
        d2 = Date("1996-12-18T12:02:00.331000", TimeSystem.UT1)
        self.assertAlmostEqual(d2.duration_from(d1), 61.8499)

    def test_date_tt(self):
        d1 = Date("2018-12-18T12:02:00.331000", TimeSystem.UTC)
        d2 = Date("2018-12-18T12:02:00.331000", TimeSystem.TT)
        self.assertAlmostEqual(d2.duration_from(d1), -69.184)

    def test_date_error(self):
        with self.assertRaises(DateError):
            Date("1996-12-18T12:02:00.331000Z", TimeSystem.TT)
        with self.assertRaises(DateError):
            Date("1996-12-18T12:02:00.331000+00:00", TimeSystem.TT)

    def test_change_scale(self):
        date = Date("2018-02-08T10:16:27.0", TimeSystem.UTC)
        date_ut1 = date.change_scale(TimeSystem.UT1)
        self.assertEqual(
            date_ut1.duration_from(date), 0.0
        )  # 0 because we compare dates in the same time scale, TAI

        date_bias_ut1 = Date("2018-02-08T10:16:27.0", TimeSystem.UT1)
        self.assertAlmostEqual(date_bias_ut1.duration_from(date_ut1), 0.0)

    def test_date_sofa_valid(self):  # example from SOFA cookbook
        Timescale.set_delta_ut1(0.3341)
        date_utc = Date("2006-01-15T21:24:37.5", TimeSystem.UTC)

        date_ut1 = Date("2006-01-15T21:24:37.834100", TimeSystem.UT1)
        self.assertAlmostEqual(date_utc.duration_from(date_ut1), 0.0)

        date_tai = Date("2006-01-15T21:25:10.500000", TimeSystem.TAI)  # TAI-UTC=  33.0
        self.assertAlmostEqual(date_utc.duration_from(date_tai), 0.0)

        date_tt = Date("2006-01-15T21:25:42.684000", TimeSystem.TT)
        self.assertAlmostEqual(date_utc.duration_from(date_tt), 0.0)

        date_tcg = Date("2006-01-15T21:25:43.322690", TimeSystem.TCG)
        self.assertAlmostEqual(date_utc.duration_from(date_tcg), 0.0, delta=1e-5)

        date_tdb = Date("2006-01-15T21:25:42.684373", TimeSystem.TDB)
        self.assertAlmostEqual(date_utc.duration_from(date_tdb), 0.0, delta=1e-5)

        date_tcb = Date("2006-01-15T21:25:56.893952", TimeSystem.TCB)
        self.assertAlmostEqual(date_utc.duration_from(date_tcb), 0.0, delta=1e-5)
