import os
import unittest

import oacmpy
from numpy import deg2rad
from oacmpy import ccsds
from oacmpy.ccsds.enumerates import (
    AttitudeFrame,
    AttitudeType,
    EulerDir,
    InterpolationMethod,
    QuaternionConvention,
)

DATA_DIR = os.path.join(oacmpy.CCSDS_DIR, "../../tests/data")


class TestAemParser(unittest.TestCase):
    def test_aem_euler_xml(self):
        object_name = "FICTITIOUS"
        message = ccsds.Aem.Aem(os.path.join(DATA_DIR, "test_aem_rotation.xml"), "xml")
        self.assertEqual(message.get_satellite_count(), 1)
        self.assertEqual(message.get_satellite_list(), {object_name})
        self.assertEqual(message.get_object_id(object_name), "2020-224A")
        self.assertEqual(message.get_ref_frame_a(object_name), AttitudeFrame.J2000)
        self.assertEqual(message.get_ref_frame_b(object_name), AttitudeFrame.SC_BODY_1)
        self.assertEqual(message.get_attitude_dir(object_name), EulerDir.A2B)
        self.assertEqual(
            message.get_attitude_type(object_name), AttitudeType.EULER_ANGLE_RATE
        )

        attitudes = message.get_attitude(object_name)

        self.assertEqual(len(attitudes), 2)
        attitude = attitudes[0]
        attitude_epoch = attitude[0]
        angles, angle_rates = attitude[1]
        self.assertEqual(attitude_epoch.str(), "2020-03-30T05:00:00.071000Z")
        self.assertEqual(angles[0], deg2rad(45.0))  # rotation 1
        self.assertEqual(angles[1], deg2rad(0.9))  # rotation 2
        self.assertEqual(angles[2], deg2rad(15.0))  # rotation 3
        self.assertEqual(angle_rates[0], deg2rad(4.5))  # rotation 1
        self.assertEqual(angle_rates[1], deg2rad(0.123))  # rotation 2
        self.assertEqual(angle_rates[2], deg2rad(15.0))  # rotation 3

    def test_aem_spin_xml(self):
        object_name = "ST5-224"
        message = ccsds.Aem.Aem(os.path.join(DATA_DIR, "test_aem_spin.xml"), "xml")
        self.assertEqual(message.get_version(), "1.0")
        self.assertEqual(message.get_satellite_count(), 1)
        self.assertEqual(message.get_satellite_list(), {object_name})
        self.assertEqual(message.get_object_id(object_name), "2006224")
        self.assertEqual(message.get_ref_frame_a(object_name), AttitudeFrame.J2000)
        self.assertEqual(message.get_ref_frame_b(object_name), AttitudeFrame.SC_BODY_1)
        self.assertEqual(message.get_attitude_dir(object_name), EulerDir.A2B)
        self.assertEqual(message.get_attitude_type(object_name), AttitudeType.SPIN)

        attitudes = message.get_attitude(object_name)

        self.assertEqual(len(attitudes), 8)
        attitude = attitudes[0]
        attitude_epoch = attitude[0]
        spin = attitude[1]
        self.assertEqual(attitude_epoch.str(), "2006-03-31T05:00:00.071000Z")
        self.assertEqual(spin[0], deg2rad(2.6862511e002))  # alpha
        self.assertEqual(spin[1], deg2rad(6.8448486e001))  # delta
        self.assertEqual(spin[2], deg2rad(1.5969509e002))  # angle
        self.assertEqual(spin[3], deg2rad(-1.0996528e002))  # angular velocity

    def test_aem_kvn(self):
        message = ccsds.Aem.Aem(os.path.join(DATA_DIR, "test_aem.aem"), "kvn")
        self.assertEqual(message.get_version(), "1.0")
        self.assertEqual(message.get_satellite_count(), 2)
        self.assertEqual(
            message.get_satellite_list(),
            {"MARS GLOBAL SURVEYOR", "mars global surveyor"},
        )

        object_name = "MARS GLOBAL SURVEYOR"
        self.assertEqual(message.get_object_id(object_name), "1996-062A")

        self.assertEqual(
            message.get_attitude_type(object_name),
            AttitudeType.QUATERNION,
        )
        self.assertEqual(
            message.get_quaternion_type(object_name),
            QuaternionConvention.LAST,
        )
        self.assertEqual(
            message.get_interpolation_method(object_name),
            InterpolationMethod.HERMITE,
        )
        self.assertEqual(message.get_interpolation_degree(object_name), 7)

        attitudes = message.get_attitude(object_name)
        self.assertEqual(len(attitudes), 4)
        attitude = attitudes[0]
        attitude_epoch = attitude[0]
        quat = attitude[1]
        self.assertEqual(attitude_epoch.str(), "1996-11-28T21:29:07.255500Z")
        self.assertEqual(quat[0], 0.56748)
        self.assertEqual(quat[1], 0.03146)
        self.assertEqual(quat[2], 0.45689)
        self.assertEqual(quat[3], 0.68427)


if __name__ == "__main__":
    unittest.main()
