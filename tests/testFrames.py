import unittest

import mock
import numpy as np
from numpy import deg2rad
from oacmpy.ccsds.enumerates import TimeSystem
from oacmpy.constants import ARCSEC_TO_RAD, MILLI_ARCSEC_TO_RAD
from oacmpy.datetime.Date import Date
from oacmpy.datetime.TimeScale import Timescale
from oacmpy.frames.frames import (
    CIRF,
    EME2000,
    GCRF,
    ITRF,
    MOD,
    PEF,
    TEME,
    TIRF,
    TOD,
    TOD2000,
)
from oacmpy.frames.iers.eop import EopDb
from oacmpy.frames.iers.iau1980 import _nutation_iau1980, _obliquity_iau1980
from oacmpy.frames.iers.iau2000 import _xys_2000A
from oacmpy.frames.iers.iau2010 import _sidereal_era_2000, _xys_2006A
from oacmpy.frames.iers.iers2003 import _luni_solar, _planets

# from src.oacmpy.frames.frames import get_frame, EME2000, TIRF, GCRF, TOD, MOD
# from src.oacmpy.ccsds.enumerates import TimeSystem
# from src.oacmpy.datetime.Date import Date

# Tolerances for validation. Validation is done against results from SOFA, Orekit and Scilab/Celestlab (PEF)
tol_pos = 0.1  # m
tol_vel = 1e-3  # m/s


class TestFramesModels(unittest.TestCase):
    # def test_era(self):
    #    date = Date("2018-03-02T15:33:00", TimeSystem.UTC)
    #    m = iau2010.sidereal_era_2000(date)
    #    self.assertEqual(epsb, 0.40905156950518634)

    def test_obliquity_iau1980(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.TT)
        self.assertEqual(date.jd, (2458180, 0.1479166666666667))
        t = date.julian_century
        eps_bar = _obliquity_iau1980(t)
        self.assertEqual(eps_bar, 0.4090515735213773)

    def test_nutation_iau1980(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.TT)
        self.assertEqual(date.jd, (2458180, 0.1479166666666667))
        self.assertEqual(date.julian_century, 0.18166044946383755)
        eop = EopDb.get(date.mjd)
        t = date.julian_century
        dpsi, deps = _nutation_iau1980(t, eop, False)
        self.assertEqual(deps, -2.8102593392228186e-05)
        self.assertEqual(dpsi, -5.609630001389791e-05)

    @unittest.skip
    def test_sofa_equinox(self):
        pass

    @unittest.skip
    def test_sofa_iau2000A_cio(self):
        pass

    @unittest.skip
    def test_sofa_iau2000A_equinox(self):
        pass

    @unittest.skip
    def test_sofa_iau2000A06_cio_angles(self):
        date = Date("2007-04-05T12:00:00", TimeSystem.UTC)
        date.set_delta_ut1(-0.072073685)
        eop = mock.Mock()
        eop.dx = 0.1750  # arcseconds
        eop.dy = -0.2650  # arcseconds
        X, Y, s = _xys_2000A(date, eop)
        self.assertAlmostEqual(X, 0.000712264729599, delta=1e-6)
        self.assertAlmostEqual(Y, 0.000044385250426, delta=1e-6)
        self.assertAlmostEqual(s / ARCSEC_TO_RAD, -0.002200475, delta=1e-6)
        self.assertAlmostEqual(
            _sidereal_era_2000(date), deg2rad(13.318492966097), delta=1e-6
        )

    def test_sofa_iua2000A_cio_series(self):
        # Reference: SOFA
        Timescale.set_delta_ut1(-0.072073685)
        date = Date("2007-04-05T12:00:00", TimeSystem.UTC)
        eop = mock.Mock()
        eop.dx = 0.1750  # arcseconds
        eop.dy = -0.2650  # arcseconds
        X, Y, s = _xys_2006A(date, eop)
        self.assertAlmostEqual(X, 0.000712264729525, delta=1e-6)
        self.assertAlmostEqual(Y, 0.000044385248875, delta=1e-6)
        self.assertAlmostEqual(s / ARCSEC_TO_RAD, -0.002200475, delta=1e-6)
        self.assertAlmostEqual(_sidereal_era_2000(date), deg2rad(13.318492966097))

    @unittest.skip
    def test_nut_argument(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.TT)
        tjc = date.julian_century
        lunisol = _luni_solar(tjc)
        planets = _planets(tjc)
        print(np.concatenate((lunisol, planets)))


class TestFramesDirect(unittest.TestCase):
    def test_tod_mod(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.TT)
        pos_vel_mod = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_tod = MOD.transform(date, pos_vel_mod, TOD)
        self.assertAlmostEqual(pos_vel_tod[0], -3899989.57522581, delta=0.1 * tol_pos)
        self.assertAlmostEqual(pos_vel_tod[1], 2500051.78369394, delta=0.1 * tol_pos)
        self.assertAlmostEqual(pos_vel_tod[2], -5299983.24449981, delta=0.1 * tol_pos)
        self.assertAlmostEqual(pos_vel_tod[3], 5539.81043070428, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[4], -1600.42013105143, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[5], -4800.07869979742, delta=tol_vel)

    def test_tod2000_mod(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.TT)
        pos_vel_mod = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_tod = MOD.transform(date, pos_vel_mod, TOD2000)
        self.assertAlmostEqual(pos_vel_tod[0], -3899989.568921, delta=0.1 * tol_pos)
        self.assertAlmostEqual(pos_vel_tod[1], 2500051.930008, delta=0.1 * tol_pos)
        self.assertAlmostEqual(pos_vel_tod[2], -5299983.180122, delta=0.1 * tol_pos)
        self.assertAlmostEqual(pos_vel_tod[3], 5539.81043070428, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[4], -1600.42013105143, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[5], -4800.07869979742, delta=tol_vel)

    def test_cirf_gcrf(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.TT)
        pos_vel_cirf = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_gcrf = CIRF.transform(date, pos_vel_cirf, GCRF)
        self.assertAlmostEqual(pos_vel_gcrf[0], -3909229.472822347)
        self.assertAlmostEqual(pos_vel_gcrf[1], 2500167.5422501327)
        self.assertAlmostEqual(pos_vel_gcrf[2], -5293116.963519325)
        self.assertAlmostEqual(pos_vel_gcrf[3], 5531.627430519052, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_gcrf[4], -1599.8478279507824, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_gcrf[5], -4809.696965228719, delta=tol_vel)

    def test_tirf_cirf(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.UT1)
        pos_vel_tirf = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]
        pos_vel_cirf = TIRF.transform(date, pos_vel_tirf, CIRF)
        self.assertAlmostEqual(pos_vel_cirf[0], -4632114.812896364)
        self.assertAlmostEqual(pos_vel_cirf[1], -59265.1680676206)
        self.assertAlmostEqual(pos_vel_cirf[2], -5300000.0)
        # self.assertAlmostEqual(pos_vel_cirf[3], 5510.329325, delta=tol_vel)  # FIXME velocity not implemented
        # self.assertAlmostEqual(pos_vel_cirf[4], 1375.545075, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_cirf[5], -4800.0, delta=tol_vel)

    def test_pef_tirf(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.TT)
        pos_vel_teme = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_tirf = PEF.transform(date, pos_vel_teme, TIRF)
        self.assertAlmostEqual(pos_vel_tirf[0], -3899999.99989652, delta=1e-3)
        self.assertAlmostEqual(pos_vel_tirf[1], 2500000.00016144, delta=1e-3)
        self.assertAlmostEqual(pos_vel_tirf[2], -5300000.0, delta=1e-3)
        self.assertAlmostEqual(pos_vel_tirf[3], 5539.99999993377, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tirf[4], -1600.00000022932, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tirf[5], -4800.0, delta=tol_vel)

    def test_teme_pef(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.UT1)
        pos_vel_teme = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_tod = TEME.transform(date, pos_vel_teme, PEF)
        self.assertAlmostEqual(pos_vel_tod[0], -1862937.65120899, delta=1e-3)
        self.assertAlmostEqual(pos_vel_tod[1], 4241398.74424793, delta=1e-3)
        self.assertAlmostEqual(pos_vel_tod[2], -5300000.0, delta=1e-3)
        # self.assertAlmostEqual(pos_vel_tod[3], 4036.19479049572, delta=tol_vel)
        # self.assertAlmostEqual(pos_vel_tod[4], -4264.3528344749, delta=tol_vel)
        # self.assertAlmostEqual(pos_vel_tod[5], -4800.0, delta=tol_vel)


@unittest.skip
class TestFramesComposition(unittest.TestCase):
    def test_teme_cirf(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.UTC)
        pos_vel_teme = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_cirf = TEME.transform(date, pos_vel_teme, CIRF)
        self.assertAlmostEqual(pos_vel_cirf[0], -3889811.69472726, delta=1e-3)
        self.assertAlmostEqual(pos_vel_cirf[1], 2515822.92293457, delta=1e-3)
        self.assertAlmostEqual(pos_vel_cirf[2], -5300000.0, delta=1e-6)
        self.assertAlmostEqual(pos_vel_cirf[3], 5533.45438403216, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_cirf[4], -1622.49273887311, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_cirf[5], -4800.0, delta=tol_vel)

    def test_teme_tod(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.UTC)
        pos_vel_teme = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_tod = TEME.transform(date, pos_vel_teme, TOD)
        self.assertAlmostEqual(pos_vel_tod[0], -3899871.568921)
        self.assertAlmostEqual(pos_vel_tod[1], 2500201.930008)
        self.assertAlmostEqual(pos_vel_tod[2], -5299999.180122)
        self.assertAlmostEqual(pos_vel_tod[3], 5539.81043070428, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[4], -1600.42013105143, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[5], -4800.07869979742, delta=tol_vel)

    def test_cio_based(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.UTC)
        pos_vel_eme2000 = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_gcrf = EME2000.transform(date, pos_vel_eme2000, GCRF)
        self.assertAlmostEqual(pos_vel_gcrf[0], -3899999.7499798415)
        self.assertAlmostEqual(pos_vel_gcrf[1], 2499999.5487286146)
        self.assertAlmostEqual(pos_vel_gcrf[2], -5300000.396840919)
        self.assertAlmostEqual(pos_vel_gcrf[3], 5540.000273, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_gcrf[4], -1600.000233, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_gcrf[5], -4799.999607, delta=tol_vel)

        pos_vel_cirf = EME2000.transform(date, pos_vel_eme2000, CIRF)
        self.assertAlmostEqual(pos_vel_cirf[0], -3890758.2965254975)
        self.assertAlmostEqual(pos_vel_cirf[1], 2499831.7895392245)
        self.assertAlmostEqual(pos_vel_cirf[2], -5306867.334127274)
        self.assertAlmostEqual(pos_vel_cirf[3], 5548.355989, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_cirf[4], -1600.151865, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_cirf[5], -4790.288066, delta=tol_vel)

        pos_vel_itrf = EME2000.transform(date, pos_vel_eme2000, ITRF)
        self.assertAlmostEqual(pos_vel_itrf[0], -1872478.10469)
        self.assertAlmostEqual(pos_vel_itrf[1], 4228600.480558)
        self.assertAlmostEqual(pos_vel_itrf[2], -5306860.06253)
        self.assertAlmostEqual(pos_vel_itrf[3], 5548.355989, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_itrf[4], -1600.151865, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_itrf[5], -4790.2888066, delta=tol_vel)

        pos_vel_tirf = EME2000.transform(date, pos_vel_eme2000, TIRF)
        self.assertAlmostEqual(pos_vel_tirf[0], -1872478.047691)
        self.assertAlmostEqual(pos_vel_tirf[1], 4228591.594697)
        self.assertAlmostEqual(pos_vel_tirf[2], -5306867.163042)
        self.assertAlmostEqual(pos_vel_tirf[3], 4059.945913, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tirf[4], -4253.251334, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tirf[5], -4790.288066, delta=tol_vel)

    def test_equinox_based(self):
        date = Date("2018-03-02T15:33:00", TimeSystem.UTC)
        pos_vel_eme2000 = [-3900000.0, 2500000.0, -5300000.0, 5540.0, -1600.0, -4800.0]

        pos_vel_mod = EME2000.transform(date, pos_vel_eme2000, MOD)
        self.assertAlmostEqual(pos_vel_mod[0], -3900762.51576)
        self.assertAlmostEqual(pos_vel_mod[1], 2484155.645064)
        self.assertAlmostEqual(pos_vel_mod[2], 5306884.446334)
        self.assertAlmostEqual(pos_vel_mod[3], -26.404348, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_mod[4], 6499.9464, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_mod[5], -0.0234277, delta=tol_vel)

        pos_vel_tod = EME2000.transform(date, pos_vel_eme2000, TOD)
        self.assertAlmostEqual(pos_vel_tod[0], -3900753.054258)
        self.assertAlmostEqual(pos_vel_tod[1], 2484207.423591)
        self.assertAlmostEqual(pos_vel_tod[2], -5306867.163051)
        self.assertAlmostEqual(pos_vel_tod[3], -26.069601, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[4], 6499.947710624858, delta=tol_vel)
        self.assertAlmostEqual(pos_vel_tod[5], -0.2054862, delta=tol_vel)
