import os
import unittest

import oacmpy
from oacmpy import ccsds
from oacmpy.ccsds.enumerates import ReferenceFrame, TimeSystem

DATA_DIR = os.path.join(oacmpy.CCSDS_DIR, "../../tests/data")


class TestOpmParser(unittest.TestCase):
    def test_opm_xml(self):
        message = ccsds.Opm.Opm(os.path.join(DATA_DIR, "test_opm.xml"), "xml")
        self.assertEqual(message.get_version(), "2.0")
        self.assertEqual(message.get_object_id(), "1998-057A")
        self.assertEqual(message.get_center_name(), "EARTH")
        self.assertEqual(message.get_reference_frame(), ReferenceFrame.ITRF97)
        self.assertEqual(message.get_time_system(), TimeSystem.UTC)

        self.assertEqual(message.get_epoch().str(), "1996-12-18T14:28:15.117200Z")
        self.assertEqual(message.get_x(), 6503514.000)
        self.assertEqual(message.get_y(), 1239647.000)
        self.assertEqual(message.get_z(), -717490.000)
        self.assertAlmostEqual(message.get_xdot(), -873.160)
        self.assertEqual(message.get_ydot(), 8740.420)
        self.assertEqual(message.get_zdot(), -4191.076)

        spacecraft_parameters = message.get_spacecraft_parameters()
        self.assertEqual(float(spacecraft_parameters["MASS"]), 3000.0)
        self.assertEqual(float(spacecraft_parameters["SOLAR_RAD_AREA"]), 18.77)
        self.assertEqual(float(spacecraft_parameters["SOLAR_RAD_COEFF"]), 1.0)
        self.assertEqual(float(spacecraft_parameters["DRAG_AREA"]), 18.77)
        self.assertEqual(float(spacecraft_parameters["DRAG_COEFF"]), 2.5)

        covariance = message.get_covariance()
        self.assertEqual(covariance[0][0], 0.316)
        self.assertEqual(covariance[3][3], 0.797)

    def test_opm_kvn(self):
        message = ccsds.Opm.Opm(os.path.join(DATA_DIR, "test_opm.opm"), "kvn")
        self.assertEqual(message.get_version(), "2.0")

        self.assertEqual(message.get_object_id(), "2000-028A")
        self.assertEqual(message.get_center_name(), "EARTH")
        self.assertEqual(message.get_reference_frame(), ReferenceFrame.TOD)
        self.assertEqual(message.get_time_system(), TimeSystem.UTC)

        self.assertEqual(message.get_x(), 6655994.2)
        self.assertEqual(message.get_y(), -40218575.1)
        self.assertEqual(message.get_z(), -82917.7)
        self.assertEqual(message.get_xdot(), 3115.48208)
        self.assertEqual(message.get_ydot(), 470.42605)
        self.assertEqual(message.get_zdot(), -1.01495)

        self.assertEqual(message.get_covariance_frame(), ReferenceFrame.RTN)
        covariance = message.get_covariance()
        self.assertEqual(covariance[0][0], [3.331349476038534e-04])
        self.assertListEqual(
            list(covariance[1][0:2]), [4.618927349220216e-04, 6.782421679971363e-04]
        )
        self.assertListEqual(
            list(covariance[2][0:3]),
            [-3.070007847730449e-04, -4.221234189514228e-04, 3.231931992380369e-04],
        )
        self.assertListEqual(
            list(covariance[3][0:4]),
            [
                -3.349365033922630e-07,
                -4.686084221046758e-07,
                2.484949578400095e-07,
                4.296022805587290e-10,
            ],
        )
        self.assertListEqual(
            list(covariance[4][0:5]),
            [
                -2.211832501084875e-07,
                -2.864186892102733e-07,
                1.798098699846038e-07,
                2.608899201686016e-10,
                1.767514756338532e-10,
            ],
        )
        self.assertListEqual(
            list(covariance[5][0:6]),
            [
                -3.041346050686871e-07,
                -4.989496988610662e-07,
                3.540310904497689e-07,
                1.869263192954590e-10,
                1.008862586240695e-10,
                6.224444338635500e-10,
            ],
        )


if __name__ == "__main__":
    unittest.main()
