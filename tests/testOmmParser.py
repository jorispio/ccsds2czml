import os
import unittest

import oacmpy
from oacmpy import ccsds
from oacmpy.ccsds.enumerates import ReferenceFrame, TimeSystem

DATA_DIR = os.path.join(oacmpy.CCSDS_DIR, "../../tests/data")


class TestOmmParser(unittest.TestCase):
    def test_omm_xml(self):
        message = ccsds.Omm.Omm(os.path.join(DATA_DIR, "test_omm.xml"), "xml")
        self.assertEqual(message.get_version(), "2.0")

        self.assertEqual(message.get_object_id(), "1995-025A")
        self.assertEqual(message.get_center_name(), "EARTH")
        self.assertEqual(message.get_reference_frame(), ReferenceFrame.TEME)
        self.assertEqual(message.get_time_system(), TimeSystem.UTC)

        self.assertEqual(message.get_mean_element_theory(), "TLE")

        covariance = message.get_covariance()
        self.assertEqual(covariance[0][0], 0.316)
        self.assertEqual(covariance[3][3], 0.797)

    def test_omm_kvn(self):
        message = ccsds.Omm.Omm(os.path.join(DATA_DIR, "test_omm.omm"), "kvn")
        self.assertEqual(message.get_version(), "2.0")

        self.assertEqual(message.get_object_id(), "1995-025A")
        self.assertEqual(message.get_center_name(), "EARTH")
        self.assertEqual(message.get_reference_frame(), ReferenceFrame.TEME)
        self.assertEqual(message.get_time_system(), TimeSystem.UTC)

        self.assertEqual(message.get_mean_element_theory(), "SGP/SGP4")

        self.assertEqual(message.get_mean_motion(), 1.00273272)

        self.assertEqual(message.get_eccentricity(), 0.0005013)
        self.assertEqual(message.get_inclination(), 3.0539)
        self.assertEqual(message.get_raan(), 81.7939)
        self.assertEqual(message.get_arg_of_pericenter(), 249.2363)
        self.assertEqual(message.get_mean_anomaly(), 150.1602)

        self.assertEqual(message.get_GM(), 398600.8)

        self.assertEqual(message.get_covariance_frame(), ReferenceFrame.TEME)
        covariance = message.get_covariance()
        self.assertEqual(covariance[0][0], [3.331349476038534e-04])
        self.assertListEqual(
            list(covariance[1][0:2]), [4.618927349220216e-04, 6.782421679971363e-04]
        )
        self.assertListEqual(
            list(covariance[2][0:3]),
            [-3.070007847730449e-04, -4.221234189514228e-04, 3.231931992380369e-04],
        )
        self.assertListEqual(
            list(covariance[3][0:4]),
            [
                -3.349365033922630e-07,
                -4.686084221046758e-07,
                2.484949578400095e-07,
                4.296022805587290e-10,
            ],
        )
        self.assertListEqual(
            list(covariance[4][0:5]),
            [
                -2.211832501084875e-07,
                -2.864186892102733e-07,
                1.798098699846038e-07,
                2.608899201686016e-10,
                1.767514756338532e-10,
            ],
        )
        self.assertListEqual(
            list(covariance[5][0:6]),
            [
                -3.041346050686871e-07,
                -4.989496988610662e-07,
                3.540310904497689e-07,
                1.869263192954590e-10,
                1.008862586240695e-10,
                6.224444338635500e-10,
            ],
        )


if __name__ == "__main__":
    unittest.main()
