import unittest
from math import cos, pi, sin, sqrt

import numpy as np
from oacmpy.czml.CdmToCzml import Covariance, RtnMatrix
from oacmpy.utils.rotation import dcm_to_quat


class TestRtnMatrix(unittest.TestCase):
    def test_rtn1(self):
        r_eci = np.array([0, 1, 0])
        v_eci = np.array([-1, 0, 0])
        rtn_to_eci = RtnMatrix(r_eci, v_eci)
        mat = rtn_to_eci.get_mat()
        self.assertEqual(mat[0][0], 0)
        self.assertEqual(mat[0][1], -1)
        self.assertEqual(mat[0][2], 0)
        self.assertEqual(mat[1][0], 1)
        self.assertEqual(mat[1][1], 0)
        self.assertEqual(mat[1][2], 0)
        self.assertEqual(mat[2][0], 0)
        self.assertEqual(mat[2][1], 0)
        self.assertEqual(mat[2][2], 1)

        u_rtn = [1, 0, 0]
        u_eci = rtn_to_eci.to_eci(u_rtn)
        self.assertEqual(u_eci[0], 0)
        self.assertEqual(u_eci[1], 1)
        self.assertEqual(u_eci[2], 0)

    def test_rtn2(self):
        r_eci = np.array([2570097.0, 2244656.9, 6281497.9])
        v_eci = np.array([4418.8, 44833.5, -3526.8])
        rtn_to_eci = RtnMatrix(r_eci, v_eci)
        ur = r_eci / np.linalg.norm(r_eci)

        u_rtn = [1, 0, 0]
        u_eci = rtn_to_eci.to_eci(u_rtn)
        self.assertEqual(u_eci[0], ur[0])
        self.assertEqual(u_eci[1], ur[1])
        self.assertEqual(u_eci[2], ur[2])


class TestQuaternion(unittest.TestCase):
    def test_dcm_to_quarternion_convention(self):
        mat = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
        quat = dcm_to_quat(mat)
        self.assertEqual(quat[0], 0)
        self.assertEqual(quat[1], 0)
        self.assertEqual(quat[2], 0)
        self.assertEqual(quat[3], 1)

    def test_dcm_to_quarternion_single_rotations(self):
        ang = pi / 6.0
        c = cos(ang)
        s = sin(ang)
        c2 = cos(ang / 2.0)

        mat = np.array([[1.0, 0.0, 0.0], [0.0, c, -s], [0.0, s, c]])
        quat = dcm_to_quat(mat)
        self.assertEqual(np.linalg.norm(quat), 1.0)
        quat_cos_angle2 = quat[3]
        quat_axis = quat[0:3] / sqrt(1 - quat_cos_angle2 * quat_cos_angle2)
        self.assertEqual(quat_cos_angle2, c2)
        self.assertAlmostEqual(quat_axis[0], 1.0)
        self.assertAlmostEqual(quat_axis[1], 0.0)
        self.assertAlmostEqual(quat_axis[2], 0.0)

        mat = np.array([[c, 0.0, -s], [0.0, 1.0, 0.0], [s, 0.0, c]])
        quat = dcm_to_quat(mat)
        self.assertEqual(np.linalg.norm(quat), 1.0)
        quat_cos_angle2 = quat[3]
        quat_axis = quat[0:3] / sqrt(1 - quat_cos_angle2 * quat_cos_angle2)
        self.assertAlmostEqual(quat_cos_angle2, c2)
        self.assertAlmostEqual(quat_axis[0], 0.0)
        self.assertAlmostEqual(quat_axis[1], -1.0)
        self.assertAlmostEqual(quat_axis[2], 0.0)

        mat = np.array([[c, -s, 0.0], [s, c, 0.0], [0.0, 0.0, 1.0]])
        quat = dcm_to_quat(mat)
        self.assertEqual(np.linalg.norm(quat), 1.0)
        quat_cos_angle2 = quat[3]
        quat_axis = quat[0:3] / sqrt(1 - quat_cos_angle2 * quat_cos_angle2)
        self.assertAlmostEqual(quat_cos_angle2, c2)
        self.assertAlmostEqual(quat_axis[0], 0.0)
        self.assertAlmostEqual(quat_axis[1], 0.0)
        self.assertAlmostEqual(quat_axis[2], 1.0)

    def test_dcm_to_quaternion_single_comp_rotations(self):
        ang = pi / 4.0
        c = cos(ang)
        s = sin(ang)
        c2 = cos(ang / 2.0)

        mat1 = np.array([[1.0, 0.0, 0.0], [0.0, c, -s], [0.0, s, c]])
        mat2 = np.array([[c, 0.0, -s], [0.0, 1.0, 0.0], [s, 0.0, c]])
        mat3 = np.array([[c, -s, 0.0], [s, c, 0.0], [0.0, 0.0, 1.0]])
        mat = np.dot(mat1, mat2, mat3)
        quat = dcm_to_quat(mat)
        self.assertAlmostEqual(np.linalg.norm(quat), 1.0)
        quat_cos_angle2 = quat[3]
        quat_axis = quat[0:3] / sqrt(1 - quat_cos_angle2 * quat_cos_angle2)
        # self.assertEqual(quat_cos_angle2, c2)
        # self.assertAlmostEqual(quat_axis[0], 1.)
        # self.assertAlmostEqual(quat_axis[1], 0.)
        # self.assertAlmostEqual(quat_axis[2], 0.)
