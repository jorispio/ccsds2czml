import os
import unittest

import oacmpy
from oacmpy import ccsds
from oacmpy.ccsds.exceptions import CcsdsError

DATA_DIR = os.path.join(oacmpy.CCSDS_DIR, "../../tests/data")


class TestCdmParser(unittest.TestCase):
    def test_cdm_kvn(self):
        message = ccsds.Cdm.Cdm(os.path.join(DATA_DIR, "test_cdm.cdm"), "kvn")
        self.assertEqual(message.get_version(), "1.0")
        self.assertEqual(message.get_satellite_count(), 2)
        self.assertEqual(
            list(message.get_satellite_list()), ["SATELLITE A", "FENGYUN 1C DEB"]
        )

        self.assertEqual(message.get_tca().str(), "2010-03-13T22:37:52.618000Z")
        self.assertEqual(message.get_miss_distance(), 715)
        with self.assertRaises(CcsdsError):
            message.get_probability()

    def test_cdm_xml(self):
        message = ccsds.Cdm.Cdm(os.path.join(DATA_DIR, "test_cdm.xml"), "xml")
        self.assertEqual(message.get_version(), "1.0")
        self.assertEqual(message.get_satellite_count(), 2)
        self.assertEqual(
            list(message.get_satellite_list()), ["SATELLITE A", "FENGYUN 1C DEB"]
        )

        self.assertEqual(message.get_tca().str(), "2010-03-13T22:37:52.618000Z")
        self.assertEqual(message.get_miss_distance(), 715)

        self.assertEqual(message.get_probability(), 4.835e-05)
        d_rtn = message.get_relative_state()
        self.assertEqual(d_rtn[0], 27.4)
        self.assertEqual(d_rtn[1], -70.2)
        self.assertEqual(d_rtn[2], 711.8)


if __name__ == "__main__":
    unittest.main()
