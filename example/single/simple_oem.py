# -*- coding: utf-8 -*-
"""
Simple transfer example to generate OEM and AEM files.

@author: Joris Olympio
"""

from math import radians

import numpy as np
import orekit
from java.lang import StringBuilder
from java.util import ArrayList as ArrayList
from orekit.pyhelpers import setup_orekit_curdir
from org.hipparchus.geometry.euclidean.threed import RotationOrder, Vector3D
from org.orekit.attitudes import LofOffset
from org.orekit.files.ccsds import OEMWriter  # , AEMWriter
from org.orekit.files.general import OrekitEphemerisFile
from org.orekit.frames import FramesFactory, ITRFVersion, LOFType, TopocentricFrame
from org.orekit.orbits import (
    EquinoctialOrbit,
    KeplerianOrbit,
    Orbit,
    OrbitType,
    PositionAngle,
)
from org.orekit.time import AbsoluteDate, TimeScalesFactory
from org.orekit.utils import Constants, IERSConventions, PVCoordinatesProvider

from ..example_utils import (
    create_atmosphere,
    create_drag,
    create_gravity_field,
    create_initial_state,
    create_propagator,
    get_earth_shape,
)

print("Initialising Orekit")
orekit.initVM()
setup_orekit_curdir()


utc = TimeScalesFactory.getUTC()
mu = Constants.WGS84_EARTH_MU

sma = 6378136.0 + 2000e3  # m
ecc = 0.000001
inc = radians(50.0)
aop = radians(0.0)
raan = radians(0.0)
anomaly = radians(0.0)
frame = FramesFactory.getEME2000()

refDate = AbsoluteDate(2020, 1, 1, 0, 0, 0.0, utc)
satellite_mass = 2000.0  # kg
crossSection = 2.5  # m2
solarArraySurf = 2 * 16 * 6.0  # m2
dragCoeff = 2.2
duration = 86400.0 * 2  # s
step_time = 600.0  # s


def create_oem():
    # initial orbit
    initial_orbit = KeplerianOrbit(
        sma, ecc, inc, aop, raan, anomaly, PositionAngle.MEAN, frame, refDate, mu
    )

    # Attitude
    attitudeLaw = LofOffset(
        initial_orbit.getFrame(),
        LOFType.TNW,
        RotationOrder.XYZ,
        radians(0.0),
        radians(0.0),
        radians(0.0),
    )

    initial_state = create_initial_state(initial_orbit, satellite_mass)
    propagator = create_propagator(
        initial_state,
        cross_section=crossSection,
        solar_array_surf=solarArraySurf,
        drag_coeff=dragCoeff,
        n_zonal=6,
        n_tesseral=2,
    )
    propagator.setAttitudeProvider(attitudeLaw)

    print("Propagating")

    # Time array in orekit AbsoluteDate format
    tt = np.arange(0, duration, step_time)
    t = [refDate.shiftedBy(float(dt)) for dt in tt]
    states = [propagator.propagate(x) for x in t]
    java_state_list = ArrayList()
    [java_state_list.add(s) for s in states]

    object_name = "Object"
    print("Generating OEM... ", object_name)
    ephemeris_file = OrekitEphemerisFile()
    satellite = ephemeris_file.addSatellite(object_name)
    satellite.addNewSegment(java_state_list)
    oem_writer = OEMWriter(
        OEMWriter.InterpolationMethod.LAGRANGE, "oacmPy", object_name, object_name
    )

    oem_str = StringBuilder()
    oem_writer.write(oem_str, ephemeris_file)
    with open("sample_object.oem", "w") as f:
        f.write(oem_str.toString())

    aem_str = StringBuilder()
    # aem_writer = AEMWriter("oacmPy", "Object1", "Object1")
    # aem_writer.write(aem_str, ephemeris_file)
    with open("sample.aem", "w") as f:
        f.write(aem_str.toString())

    print("Done")


if __name__ == "__main__":
    create_oem()
