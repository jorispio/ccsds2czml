To run the examples, you will need to:
 - install orekit python to run the examples.
 - download the orekit data file _orekit-data.zip_ and place it in the example folder
 
All the examples have to be run from the parent root directory.

### Simple Orbit

Running:

    python -m example.single.simple_oem
    
will create one OEM file _sample_object.oem_ with the satellite orbit. 

Create a CZML with (we could as well do it directly in the Python script though ;)):

    python -m oacmpy -i sample_object.oem -o one_sat.czml -v

![simple orbit](./single/screenshot.png)


### Constellation
The object of this example is to create a single CZML file from multiple OEM, and display ground station links.

Running:

    python -m example.constellation.constellation_oems
    
will create a set of OEM files _sample_object*.oem_, and a CZML Cesium scene file.
The scene file includes the satellite visibilities from the selected ground station (Svalbard). 
A file _event.txt_ is also describing those events. 

To re-create a CZML, you can use:

    python -m oacmpy -i sample_object*.oem -o constellation.czml -v

Note the use of the wildcard.

![constellation](./constellation/screenshot.png)


### Conjunction simulation from CDM
Running:

    python -m example.conjunction.oem_from_cdm.py
        
will parse the CDM file present in the conjunction sub-directory, and back-propagate the state of both objects in conjunction from the date of TCA to TCA-45 min.
The script will then create two OEM files, and a CZML file.
    
Note that the ellipsoid is displayed from TCA-30min, but its size and attitude have not been updated during the propagation. The geometry is only relevant close to the TCA.

![conjunction between a satellite and a passive debris](./conjunction/screenshot.png)

