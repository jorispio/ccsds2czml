# -*- coding: utf-8 -*-
"""
Simulation of small constellation dynamics with ground station link.

@author: Joris Olympio
"""

from math import radians

import numpy as np
import orekit
from java.lang import StringBuilder
from java.util import ArrayList as ArrayList
from numpy import deg2rad
from orekit.pyhelpers import setup_orekit_curdir
from org.hipparchus.geometry.euclidean.threed import RotationOrder, Vector3D
from org.orekit.attitudes import LofOffset
from org.orekit.bodies import (
    CelestialBody,
    CelestialBodyFactory,
    GeodeticPoint,
    OneAxisEllipsoid,
)
from org.orekit.files.ccsds import OEMWriter  # , AEMWriter
from org.orekit.files.general import OrekitEphemerisFile
from org.orekit.frames import FramesFactory, ITRFVersion, LOFType, TopocentricFrame
from org.orekit.orbits import (
    EquinoctialOrbit,
    KeplerianOrbit,
    Orbit,
    OrbitType,
    PositionAngle,
)
from org.orekit.propagation.events import ElevationDetector
from org.orekit.time import AbsoluteDate, TimeScalesFactory
from org.orekit.utils import Constants, IERSConventions, PVCoordinatesProvider
from src.oacmpy.ccsds.Oem import Oem
from src.oacmpy.czml.CzmlUtils import add_groundstation_visi
from src.oacmpy.czml.OemToCzml import OemAemToCzml
from src.oacmpy.czml.ScenarioCzml import ScenarioCzml

from ..example_utils import create_initial_state, create_propagator, get_earth_shape
from .GroundStation import GroundStation
from .handler_groundstation import GroundstationHandler

utc = TimeScalesFactory.getUTC()
mu = Constants.WGS84_EARTH_MU

sma = 6378136.0 + 2000e3  # m
ecc = 0.000001
inc = radians(50.0)
aop = radians(0.0)
anomaly = radians(0.0)
frame = FramesFactory.getEME2000()

refDate = AbsoluteDate(2020, 1, 1, 0, 0, 0.0, utc)
satellite_mass = 2000.0  # kg
crossSection = 2.5  # m2
solarArraySurf = 2 * 16 * 6.0  # m2
dragCoeff = 2.2
duration = 86400.0 * 2  # s
step_time = 600.0  # s

# ground station
ground_stations = [GroundStation("Svalbard", 78.228, 15.399, 0.0)]


def addGroundStation(propagator, ground_station, elevation=radians(5.0)):
    """Create an Orekit event detector for ground station visibility"""
    point = GeodeticPoint(
        radians(ground_station.latitude),
        radians(ground_station.longitude),
        ground_station.altitude,
    )
    earthShape = get_earth_shape()
    topoframe = TopocentricFrame(earthShape, point, ground_station.name)
    staVisi = ElevationDetector(topoframe)
    staVisi = staVisi.withConstantElevation(elevation)

    handler = GroundstationHandler(ground_station)
    detector = staVisi.withHandler(handler)
    propagator.addEventDetector(detector)

    handler.station_name = ground_station.name
    return handler


def create_oem(i_object, raan):
    object_name = "Object{:d}".format(i_object)
    filename = "sample_object_{:d}.oem".format(i_object)

    # initial orbit
    initial_orbit = KeplerianOrbit(
        sma, ecc, inc, aop, raan, anomaly, PositionAngle.MEAN, frame, refDate, mu
    )

    # Attitude
    attitudeLaw = LofOffset(
        initial_orbit.getFrame(),
        LOFType.TNW,
        RotationOrder.XYZ,
        radians(0.0),
        radians(0.0),
        radians(0.0),
    )

    initial_state = create_initial_state(initial_orbit, satellite_mass)
    propagator = create_propagator(
        initial_state,
        cross_section=crossSection,
        solar_array_surf=solarArraySurf,
        drag_coeff=dragCoeff,
        n_zonal=6,
        n_tesseral=2,
    )
    propagator.setAttitudeProvider(attitudeLaw)

    #
    handlers = []
    if ground_stations:
        for gs in ground_stations:
            handler = addGroundStation(propagator, gs)
            handler.satellite_name = object_name
            handlers.append(handler)

    print("Propagating")

    # Time array in orekit AbsoluteDate format
    tt = np.arange(0, duration, step_time)
    t = [refDate.shiftedBy(float(dt)) for dt in tt]
    states = [propagator.propagate(x) for x in t]
    java_state_list = ArrayList()
    [java_state_list.add(s) for s in states]

    print("Generating OEM... ", object_name)
    ephemeris_file = OrekitEphemerisFile()
    satellite = ephemeris_file.addSatellite(object_name)
    satellite.addNewSegment(java_state_list)
    oem_writer = OEMWriter(
        OEMWriter.InterpolationMethod.LAGRANGE, "oacmPy", object_name, object_name
    )

    oem_str = StringBuilder()
    oem_writer.write(oem_str, ephemeris_file)
    with open(filename, "w") as f:
        f.write(oem_str.toString())

    return filename, handlers


def main():
    oems = []
    handlers = []
    for i in range(0, 8):
        filename, handler = create_oem(i, float(deg2rad(360.0 / 10.0 * i)))
        handlers.append(handler)
        oems.append(Oem(filename, "kvn"))

    start = oems[0].get_start_time("Object0")
    end = oems[0].get_stop_time("Object0")
    scenario = ScenarioCzml(start, end)

    for oem in oems:
        oem_czml = OemAemToCzml(oem)
        scenario.add_content(oem_czml)

    if ground_stations:
        scenario.set_groundstations(ground_stations)
        parent = 0
        events_str_lines = []
        for sat_event_handler in handlers:  # handlers per sat
            for gs_event_handler in sat_event_handler:  # one handler per gs
                scenario.add_content(
                    add_groundstation_visi(
                        gs_event_handler.station_name,
                        gs_event_handler.satellite_name,
                        parent,
                        start,
                        end,
                        gs_event_handler.events,
                    )
                )
                for e in gs_event_handler.events:
                    events_str_lines.append(
                        "{:10s} {:10s} {:s} {:s}\n".format(
                            gs_event_handler.station_name,
                            gs_event_handler.satellite_name,
                            e[0],
                            e[1],
                        )
                    )

        print("Writing events.txt")
        with open("events.txt", "w") as f:
            f.writelines(events_str_lines)

    print("Writing CZML")
    scenario.create_document("constellation_sats.czml")

    print("Done")


if __name__ == "__main__":
    main()
