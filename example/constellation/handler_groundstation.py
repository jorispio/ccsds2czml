#
from org.hipparchus.ode.events import Action
from org.orekit.frames import FramesFactory, ITRFVersion, LOFType
from org.orekit.orbits import (
    EquinoctialOrbit,
    KeplerianOrbit,
    Orbit,
    OrbitType,
    PositionAngle,
)
from org.orekit.propagation.events.handlers import EventHandler, PythonEventHandler
from org.orekit.time import AbsoluteDate, TimeScalesFactory
from org.orekit.utils import Constants


#
class GroundstationHandler(PythonEventHandler):
    ground_station = []
    events = []
    eventOn = None
    eventOff = None

    def __init__(self, ground_station):
        PythonEventHandler.__init__(self)
        self.ground_station = ground_station
        self.events = []
        self.eventOn = None

    def init(self, s, date):
        pass

    def setGroundStation(self, ground_station):
        self.ground_station = ground_station

    def eventOccurred(self, s, detector, increasing):
        if not increasing:
            self.eventOff = s.getDate()
            if self.eventOn:
                self.events.append(
                    [self.eventOn.toString() + "Z", self.eventOff.toString() + "Z"]
                )
            self.eventOn = None
        else:
            self.eventOn = s.getDate()
        return Action.CONTINUE

    def resetState(self, detector, s):
        return s
