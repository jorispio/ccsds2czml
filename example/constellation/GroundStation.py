#


class GroundStation:
    def __init__(self, name, latitude, longitude, altitude):
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude

    def get_latitude(self):
        return self.latitude

    def get_longitudeself(self):
        return self.longitude

    def get_altitude(self):
        return self.altitude

    def to_czml(self):
        return [self.longitude, self.latitude, self.altitude]

    def to_topojson(self):
        # TODO return json of ground circle
        return None
