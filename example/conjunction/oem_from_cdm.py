# -*- coding: utf-8 -*-
"""
Simulation of the conjunction event (IRIDIUM and 13 FENGYUN 1C DEB) using CDM file.

@author: Joris Olympio
"""

from datetime import timedelta
from math import radians

import numpy as np
from java.lang import StringBuilder
from java.util import ArrayList as ArrayList
from java.util import Collections
from org.hipparchus.geometry.euclidean.threed import RotationOrder, Vector3D
from org.orekit.attitudes import LofOffset
from org.orekit.files.ccsds import OEMWriter
from org.orekit.files.general import OrekitEphemerisFile
from org.orekit.frames import FramesFactory, LOFType
from org.orekit.orbits import CartesianOrbit
from org.orekit.time import AbsoluteDate, TimeScalesFactory
from org.orekit.utils import (
    AbsolutePVCoordinates,
    Constants,
    IERSConventions,
    PVCoordinates,
)
from src.oacmpy.ccsds.Cdm import Cdm
from src.oacmpy.ccsds.enumerates import ConjunctionObject, ReferenceFrame
from src.oacmpy.ccsds.Oem import Oem
from src.oacmpy.czml.CdmToCzml import CdmToCzml
from src.oacmpy.czml.OemToCzml import OemAemToCzml
from src.oacmpy.czml.ScenarioCzml import ScenarioCzml

from ..example_utils import create_initial_state, create_propagator

utc = TimeScalesFactory.getUTC()
mu = Constants.WGS84_EARTH_MU

satellite_mass = 2000.0  # kg
crossSection = 2.5  # m2
solarArraySurf = 2 * 16 * 6.0  # m2
dragCoeff = 2.2
step_time = 120.0  # s


def get_frame(frame):
    switcher = {
        ReferenceFrame.EME2000: FramesFactory.getEME2000(),
        ReferenceFrame.GTOD: FramesFactory.getGTOD(True),
        ReferenceFrame.ITRF2000: FramesFactory.getITRF(IERSConventions.IERS_2010, True),
        ReferenceFrame.ITRF93: FramesFactory.getITRF(IERSConventions.IERS_1996, True),
        ReferenceFrame.ITRF97: FramesFactory.getITRF(IERSConventions.IERS_1996, True),
        ReferenceFrame.J2000: FramesFactory.getEME2000(),
        ReferenceFrame.TEME: FramesFactory.getTEME(),
        ReferenceFrame.ICRF: FramesFactory.getICRF(),
    }
    return switcher.get(frame, "Invalid frame")


def create_oem(
    i_object, object_name, datetime, state, frame, prop_duration, duration_after_tca=0.0
):
    """create oem file"""
    position = Vector3D(state[0], state[1], state[2])
    velocity = Vector3D(state[3], state[4], state[5])
    pv = PVCoordinates(position, velocity)
    date = datetime.date()
    time = datetime.time()
    print("TCA: ", datetime)
    tca_date = AbsoluteDate(
        date.year, date.month, date.day, time.hour, time.minute, float(time.second), utc
    )

    orekit_frame = get_frame(frame)
    initial_orbit = CartesianOrbit(pv, orekit_frame, tca_date, mu)

    # Attitude
    attitude_law = LofOffset(
        initial_orbit.getFrame(),
        LOFType.TNW,
        RotationOrder.XYZ,
        radians(0.0),
        radians(0.0),
        radians(0.0),
    )

    initial_state = create_initial_state(initial_orbit, satellite_mass)
    propagator = create_propagator(
        initial_state,
        cross_section=crossSection,
        solar_array_surf=solarArraySurf,
        drag_coeff=dragCoeff,
        n_zonal=6,
        n_tesseral=2,
    )
    propagator.setAttitudeProvider(attitude_law)

    print("Propagating")

    # Time array in orekit AbsoluteDate format
    tt = np.arange(duration_after_tca, prop_duration, -step_time)
    t = [tca_date.shiftedBy(float(dt)) for dt in tt]
    states = [propagator.propagate(x) for x in t]
    java_state_list = ArrayList()
    [java_state_list.add(s) for s in states]
    Collections.reverse(java_state_list)

    print("Generating OEM... ", object_name)
    ephemeris_file = OrekitEphemerisFile()
    satellite = ephemeris_file.addSatellite(object_name)
    satellite.addNewSegment(java_state_list)
    oem_writer = OEMWriter(
        OEMWriter.InterpolationMethod.LAGRANGE, "oacmPy", object_name, object_name
    )

    oem_str = StringBuilder()
    oem_writer.write(oem_str, ephemeris_file)
    filename = "sample_object{:d}.oem".format(i_object)
    with open(filename, "w") as f:
        f.write(oem_str.toString())
    return filename


def analyse_cdm(cdm_filename):
    cdm = Cdm(cdm_filename, "xml")
    tca_date = cdm.get_tca()

    primary_name = cdm.get_primary_satellite_name()
    state = cdm.get_state_vector(obj_name=ConjunctionObject.OBJECT1)
    frame = cdm.get_coordinate_system(obj_name=ConjunctionObject.OBJECT1)
    filename1 = create_oem(
        1, primary_name, tca_date, state, frame, -45 * 60.0, 15 * 60.0
    )

    secondary_name = cdm.get_secondary_satellite_name()
    state = cdm.get_state_vector(obj_name=ConjunctionObject.OBJECT2)
    frame = cdm.get_coordinate_system(obj_name=ConjunctionObject.OBJECT2)
    filename2 = create_oem(
        2, secondary_name, tca_date, state, frame, -45 * 60.0, 15 * 60.0
    )

    oem1 = Oem(filename1, "kvn")
    oem1_czml = OemAemToCzml(oem1)
    oem2 = Oem(filename2, "kvn")
    oem2_czml = OemAemToCzml(oem2)
    cdm = Cdm(cdm_filename, "xml")
    cdm_czml = CdmToCzml(cdm)
    cdm_czml.set_lead_time(timedelta(minutes=30))
    cdm_czml.set_scale(1.0)

    start = oem1.get_start_time(sat_name="SATELLITE A")
    end = oem1.get_stop_time(sat_name="SATELLITE A")
    print("Time frame: ", start, end)

    scenario = ScenarioCzml(start, end)
    scenario.set_clock_current_time(tca_date.str())
    scenario.sef_clock_multiplier(10)
    scenario.add_content(oem1_czml)
    scenario.add_content(oem2_czml)
    scenario.add_content(cdm_czml)
    scenario.create_document("test_conjunction.czml")

    print("Done")


if __name__ == "__main__":
    cdm_filename = "example/conjunction/test_cdm.xml"
    analyse_cdm(cdm_filename)
